package com.walmart.example.products.ui

import android.app.Application
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.android.example.paging.pagingwithnetwork.R
import com.walmart.example.products.DefaultServiceLocator
import com.walmart.example.products.ServiceLocator
import com.walmart.example.products.api.ProductService
import com.walmart.example.products.data.MockProductService
import com.walmart.example.products.data.ProductsFactory
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ProductsActivityTest {

    private val productFactory = ProductsFactory()

    @Before
    fun init() {
        val mockService = MockProductService()
        mockService.addProduct(productFactory.createProduct())
        mockService.addProduct(productFactory.createProduct())
        mockService.addProduct(productFactory.createProduct())
        val app = ApplicationProvider.getApplicationContext<Application>()
        ServiceLocator.swap(
            object : DefaultServiceLocator(
                app,
                true
            ) {
                override fun getApiService(): ProductService = mockService
            }
        )
    }

    @Test
    fun showSomeResults() {
        ActivityScenario.launch<ProductsActivity>(
            ProductsActivity.intentFor(context = ApplicationProvider.getApplicationContext()).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
        )

        onView(withId(R.id.list)).check { view, noViewFoundException ->
            if (noViewFoundException != null) {
                throw noViewFoundException
            }

            val recyclerView = view as RecyclerView
            assertEquals(4, recyclerView.adapter?.itemCount)
        }
    }
}