package com.walmart.example.products.data

import com.walmart.example.products.api.ProductService
import com.walmart.example.products.api.ProductsResponse
import com.walmart.example.products.model.Product
import java.io.IOException


class MockProductService : ProductService {

    private val model = mutableMapOf<String, Product>()
    var failureMsg: String? = null

    fun addProduct(product: Product) {
        model.getOrPut(product.productId) {
            product
        }
    }

    override suspend fun getProducts(
        pageNumber: Int,
        pageSize: Int
    ): ProductsResponse {
        failureMsg?.let {
            throw IOException(it)
        }

        return ProductsResponse(
            model.values.toList(),
            pageNumber,
            200
        )
    }
}