package com.walmart.example.products.data

import com.walmart.example.products.model.Product
import java.util.concurrent.atomic.AtomicInteger

class ProductsFactory {

    private val counter = AtomicInteger(0)

    fun createProduct(): Product {
        val id = counter.incrementAndGet()
        return Product(
            "id_$id",
            "name_$id",
            "short_desc_$id",
            "long_desc_$id",
            "$49.99",
            "/images/image1.jpeg",
            4.4f,
            30,
            true
        )
    }
}