package com.walmart.example.products.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.walmart.example.products.model.RemoteKey

@Dao
interface RemoteKeyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRemoteKey(remoteKey: RemoteKey)

    @Query("DELETE FROM remote_key")
    suspend fun clearRemoteKey()

    @Query("SELECT * FROM remote_key WHERE id LIKE 'Product'")
    suspend fun remoteKey(): RemoteKey
}