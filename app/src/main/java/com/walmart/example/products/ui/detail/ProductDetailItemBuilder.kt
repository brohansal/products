package com.walmart.example.products.ui.detail

import android.text.Html
import com.android.example.paging.pagingwithnetwork.R
import com.walmart.example.products.model.Product
import com.walmart.example.products.util.ProductImageUtil

class ProductDetailItemBuilder {

    fun build(product: Product): ProductDetailItem {
        val description = when {
            !product.longDescription.isNullOrEmpty() -> Html.fromHtml(product.longDescription)
            !product.shortDescription.isNullOrEmpty() -> Html.fromHtml(product.shortDescription)
            else -> ""
        }

        return ProductDetailItem(
                product.productName,
                ProductImageUtil.getResolvedProductImage(product.productImage),
                ProductImageUtil.getImageTransitionId(product),
                product.reviewRating,
                product.reviewCount,
                product.price,
                if (product.inStock) R.string.in_stock else R.string.currently_unavailable,
                if (product.inStock) R.color.green else R.color.red,
                description
        )
    }
}