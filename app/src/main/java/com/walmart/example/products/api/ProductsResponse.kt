package com.walmart.example.products.api

import com.google.gson.annotations.SerializedName
import com.walmart.example.products.model.Product

data class ProductsResponse(
        @SerializedName("products")
        val product: List<Product>,
        @SerializedName("pageNumber")
        val pageNumber: Int,
        @SerializedName("totalProducts")
        val totalProducts: Int
)