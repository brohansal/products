package com.walmart.example.products.db

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.walmart.example.products.model.Product
import kotlinx.coroutines.flow.Flow

@Dao
interface ProductsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProducts(products: List<Product>)

    @Query("DELETE FROM products")
    suspend fun clearProducts()

    @Query("SELECT * FROM products ORDER BY id ASC")
    fun retrieveProducts(): PagingSource<Int, Product>

    @Query("SELECT * FROM products WHERE productId LIKE :productId")
    fun retrieveProduct(productId: String): Flow<Product>
}