package com.walmart.example.products.ui.detail

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.SharedElementCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.savedstate.SavedStateRegistryOwner
import com.android.example.paging.pagingwithnetwork.R
import com.android.example.paging.pagingwithnetwork.databinding.FragmentProductDetailBinding
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.stfalcon.imageviewer.StfalconImageViewer
import com.walmart.example.products.GlideApp
import com.walmart.example.products.ServiceLocator
import kotlinx.coroutines.flow.collectLatest
import java.util.concurrent.TimeUnit

class ProductDetailFragment : Fragment() {

    private class ProductDetailViewModelFactory(
            private val context: Context,
            private val productId: String,
            owner: SavedStateRegistryOwner,
            defaultArgs: Bundle? = null
    ) : AbstractSavedStateViewModelFactory(
            owner,
            defaultArgs
    ) {

        override fun <T : ViewModel?> create(
                key: String,
                modelClass: Class<T>,
                handle: SavedStateHandle
        ): T {
            val repo = ServiceLocator.instance(context).getRepository()
            @Suppress("UNCHECKED_CAST")
            return ProductDetailViewModel(
                    repo,
                    productId,
                    handle,
            ) as T
        }
    }

    private val model: ProductDetailViewModel by viewModels {
        ProductDetailViewModelFactory(
                this.requireContext(),
                requireArguments().getString(ARG_PRODUCT_ID)!!,
                this
        )
    }

    private lateinit var binding: FragmentProductDetailBinding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductDetailBinding.inflate(
                inflater,
                container,
                false)
        prepareSharedElementTransition()
        if (savedInstanceState == null) {
            postponeEnterTransition(
                    400,
                    TimeUnit.MILLISECONDS
            )
        }
        return binding.root
    }

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?
    ) {
        super.onViewCreated(
                view,
                savedInstanceState
        )
        requireActivity().title = ""
        val glide = GlideApp.with(this)
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            model.productDetailItem.collectLatest { productDetailItem ->
                binding.image.transitionName = productDetailItem.productImageTransitionId
                glide.load(productDetailItem.productImage)
                        .fitCenter()
                        .placeholder(R.drawable.bg_product)
                        .listener(object : RequestListener<Drawable?> {
                            override fun onLoadFailed(
                                    e: GlideException?,
                                    model: Any,
                                    target: Target<Drawable?>,
                                    isFirstResource: Boolean
                            ): Boolean {
                                startPostponedEnterTransition()
                                return false
                            }

                            override fun onResourceReady(
                                    resource: Drawable?,
                                    model: Any,
                                    target: Target<Drawable?>,
                                    dataSource: DataSource,
                                    isFirstResource: Boolean
                            ): Boolean {
                                startPostponedEnterTransition()
                                return false
                            }
                        })
                        .into(binding.image)
                binding.image.setOnClickListener {
                    StfalconImageViewer.Builder(
                            requireActivity(),
                            listOf(productDetailItem.productImage)
                    ) { view, image ->
                        glide.load(image).into(view)
                    }.withTransitionFrom(binding.image).withHiddenStatusBar(false).show()
                }
                binding.name.text = productDetailItem.productName
                binding.rating.rating = productDetailItem.reviewRating
                binding.ratingCount.text = productDetailItem.reviewRating.toString()
                binding.reviewCount.text = resources.getQuantityString(
                        R.plurals.reviews_with_count,
                        productDetailItem.reviewCount,
                        productDetailItem.reviewCount
                )
                binding.price.text = productDetailItem.price
                binding.availability.text = getString(productDetailItem.inStockStringRes)
                binding.availability.setTextColor(ContextCompat.getColor(
                        requireActivity(),
                        productDetailItem.inStockColorRes
                ))
                binding.description.text = productDetailItem.productDescription
            }
        }
    }

    private fun prepareSharedElementTransition() {
        val transition = TransitionInflater.from(context).inflateTransition(R.transition.image_transition)
        sharedElementEnterTransition = transition

        setEnterSharedElementCallback(
                object : SharedElementCallback() {
                    override fun onMapSharedElements(
                            names: List<String>,
                            sharedElements: MutableMap<String, View>
                    ) {
                        sharedElements[names[0]] = binding.image
                    }
                })
    }

    companion object {

        const val TAG = "ProductDetailFragment"
        private const val ARG_PRODUCT_ID = "product_id"

        fun getInstance(productId: String): ProductDetailFragment {
            return ProductDetailFragment().apply {
                val args = Bundle().apply {
                    putString(
                            ARG_PRODUCT_ID,
                            productId
                    )
                }
                arguments = args
            }
        }
    }
}