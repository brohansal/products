package com.walmart.example.products.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.walmart.example.products.api.ProductService
import com.walmart.example.products.api.ProductService.Companion.MAX_PAGE_SIZE
import com.walmart.example.products.db.ProductsDb
import com.walmart.example.products.model.Product
import kotlinx.coroutines.flow.Flow

class ProductRepository(
        private val db: ProductsDb,
        private val api: ProductService
) {
    @OptIn(ExperimentalPagingApi::class)
    fun getProducts() = Pager(
            config = PagingConfig(MAX_PAGE_SIZE),
            remoteMediator = PageKeyedRemoteMediator(
                    db,
                    api
            )
    ) {
        db.products().retrieveProducts()
    }.flow

    @OptIn(ExperimentalPagingApi::class)
    fun getProduct(productId: String): Flow<Product> {
        return db.products().retrieveProduct(productId)
    }
}