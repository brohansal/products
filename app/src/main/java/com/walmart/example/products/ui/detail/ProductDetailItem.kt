package com.walmart.example.products.ui.detail

import androidx.annotation.ColorRes
import androidx.annotation.StringRes

data class ProductDetailItem(
        val productName: String,
        val productImage: String,
        val productImageTransitionId: String,
        val reviewRating: Float,
        val reviewCount: Int,
        val price: String,
        @StringRes val inStockStringRes: Int,
        @ColorRes val inStockColorRes: Int,
        val productDescription: CharSequence
)