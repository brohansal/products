package com.walmart.example.products.model

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity(tableName = "products", indices = [Index(value = ["productId"], unique = true)])
data class Product(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        @SerializedName("productId")
        val productId: String,
        @SerializedName("productName")
        val productName: String,
        @SerializedName("shortDescription")
        val shortDescription: String?,
        @SerializedName("longDescription")
        val longDescription: String?,
        @SerializedName("price")
        val price: String,
        @SerializedName("productImage")
        val productImage: String,
        @SerializedName("reviewRating")
        val reviewRating: Float,
        @SerializedName("reviewCount")
        val reviewCount: Int,
        @SerializedName("inStock")
        val inStock: Boolean
)