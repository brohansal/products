package com.walmart.example.products.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadState.Error
import androidx.paging.LoadState.Loading
import androidx.recyclerview.widget.RecyclerView
import com.android.example.paging.pagingwithnetwork.R
import com.android.example.paging.pagingwithnetwork.databinding.LoadingStateItemBinding

class LoadingStateItemViewHolder(
        parent: ViewGroup,
        private val retryCallback: () -> Unit
) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(
                R.layout.loading_state_item,
                parent,
                false
        )
) {
    private val binding = LoadingStateItemBinding.bind(itemView)
    private val progressBar = binding.progressBar
    private val retry = binding.retryButton
            .also {
                it.setOnClickListener { retryCallback() }
            }

    fun bindTo(loadState: LoadState) {
        progressBar.isVisible = loadState is Loading
        retry.isVisible = loadState is Error
    }
}
