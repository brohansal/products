package com.walmart.example.products.ui

import android.transition.TransitionSet
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.android.example.paging.pagingwithnetwork.R
import com.walmart.example.products.GlideRequests
import com.walmart.example.products.ui.detail.ProductDetailFragment
import java.util.concurrent.atomic.AtomicBoolean

class ProductsAdapter(
        fragment: Fragment,
        private val glide: GlideRequests
) : PagingDataAdapter<ProductItem, ProductsViewHolder>(PRODUCT_ITEM_COMPARATOR) {

    private val viewHolderListener: ViewHolderListener

    init {
        viewHolderListener = ViewHolderListenerImpl(fragment)
    }

    override fun onBindViewHolder(
            holder: ProductsViewHolder,
            position: Int
    ) {
        holder.bind(getItem(position)!!)
    }

    override fun onViewRecycled(holder: ProductsViewHolder) {
        super.onViewRecycled(holder)
        holder.unbind()
    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): ProductsViewHolder {
        return ProductsViewHolder.create(
                parent,
                glide,
                viewHolderListener
        )
    }

    interface ViewHolderListener {
        fun onLoadCompleted(
                view: ImageView,
                adapterPosition: Int
        )

        fun onItemClicked(
                view: View,
                adapterPosition: Int,
                productId: String
        )
    }

    private class ViewHolderListenerImpl constructor(private val fragment: Fragment) : ViewHolderListener {
        private val enterTransitionStarted: AtomicBoolean = AtomicBoolean()

        override fun onLoadCompleted(
                view: ImageView,
                adapterPosition: Int
        ) {
            if (ProductsActivity.currentPosition != adapterPosition) {
                return
            }
            if (enterTransitionStarted.getAndSet(true)) {
                return
            }
            fragment.startPostponedEnterTransition()
        }

        override fun onItemClicked(
                view: View,
                adapterPosition: Int,
                productId: String
        ) {
            ProductsActivity.currentPosition = adapterPosition

            (fragment.exitTransition as TransitionSet).excludeTarget(
                    view,
                    true
            )
            val transitioningView = view.findViewById<ImageView>(R.id.image)
            fragment.parentFragmentManager.commit {
                val fragment = ProductDetailFragment.getInstance(productId)
                setReorderingAllowed(true)
                addSharedElement(
                        transitioningView,
                        transitioningView.transitionName
                )
                replace(
                        R.id.fragment_container_view,
                        fragment,
                        ProductDetailFragment.TAG
                )
                addToBackStack(null)
            }
        }
    }

    companion object {
        val PRODUCT_ITEM_COMPARATOR = object : DiffUtil.ItemCallback<ProductItem>() {
            override fun areContentsTheSame(
                    oldItem: ProductItem,
                    newItem: ProductItem
            ): Boolean = oldItem == newItem

            override fun areItemsTheSame(
                    oldItem: ProductItem,
                    newItem: ProductItem
            ): Boolean = oldItem.productId == newItem.productId
        }
    }
}