package com.walmart.example.products.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "remote_key")
data class RemoteKey(
        @PrimaryKey
        val id: String = "Product",
        val nextPage: Int?
)