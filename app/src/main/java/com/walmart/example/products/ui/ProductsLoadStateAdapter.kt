package com.walmart.example.products.ui

import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter

class ProductsLoadStateAdapter(
        private val adapter: ProductsAdapter
) : LoadStateAdapter<LoadingStateItemViewHolder>() {
    override fun onBindViewHolder(
            holder: LoadingStateItemViewHolder,
            loadState: LoadState
    ) {
        holder.bindTo(loadState)
    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            loadState: LoadState
    ): LoadingStateItemViewHolder {
        return LoadingStateItemViewHolder(parent) { adapter.retry() }
    }
}