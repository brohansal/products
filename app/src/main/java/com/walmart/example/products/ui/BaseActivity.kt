package com.walmart.example.products.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.android.example.paging.pagingwithnetwork.R

open class BaseActivity : AppCompatActivity(), FragmentManager.OnBackStackChangedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.addOnBackStackChangedListener(this)
        updateToolbar()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (canPopBackStack()) {
                    supportFragmentManager.popBackStack()
                    return true
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackStackChanged() {
        updateToolbar()
    }

    private fun updateToolbar() {
        val canPopStack = canPopBackStack()
        supportActionBar!!.apply {
            setHomeButtonEnabled(canPopStack)
            setDisplayHomeAsUpEnabled(canPopStack)
        }
        if (!canPopStack) {
            title = getString(R.string.app_name)
        }
    }

    private fun canPopBackStack() = supportFragmentManager.backStackEntryCount > 0
}