package com.walmart.example.products.ui

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.walmart.example.products.data.ProductRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.flattenMerge
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.receiveAsFlow

class ProductsViewModel(
        private val repository: ProductRepository,
        private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val productItemBuilder = ProductItemBuilder()
    private val clearListCh = Channel<Unit>(Channel.CONFLATED)

    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    val productItems = flowOf(
            clearListCh.receiveAsFlow().map { PagingData.empty<ProductItem>() },
            repository.getProducts().map {
                it.map { product ->
                    productItemBuilder.build(product)
                }
            }.cachedIn(viewModelScope)
    ).flattenMerge(2)

    fun showProducts() {
        clearListCh.offer(Unit)
    }
}
