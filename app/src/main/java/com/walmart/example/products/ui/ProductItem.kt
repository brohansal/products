package com.walmart.example.products.ui

import androidx.annotation.ColorRes
import androidx.annotation.StringRes

data class ProductItem(
        val productId: String,
        val productName: String,
        val productImage: String,
        val productImageTransitionId: String,
        val reviewRating: Float,
        val price: String,
        @StringRes val inStockStringRes: Int,
        @ColorRes val inStockColorRes: Int
)