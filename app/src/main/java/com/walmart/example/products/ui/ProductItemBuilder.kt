package com.walmart.example.products.ui

import com.android.example.paging.pagingwithnetwork.R
import com.walmart.example.products.model.Product
import com.walmart.example.products.util.ProductImageUtil

class ProductItemBuilder {

    fun build(product: Product): ProductItem {
        return ProductItem(
                product.productId,
                product.productName,
                ProductImageUtil.getResolvedProductImage(product.productImage),
                ProductImageUtil.getImageTransitionId(product),
                product.reviewRating,
                product.price,
                if (product.inStock) R.string.in_stock else R.string.currently_unavailable,
                if (product.inStock) R.color.green else R.color.red,
        )
    }
}