package com.walmart.example.products.ui

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.SharedElementCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.android.example.paging.pagingwithnetwork.R
import com.android.example.paging.pagingwithnetwork.databinding.FragmentProductsBinding
import com.walmart.example.products.GlideApp
import com.walmart.example.products.ServiceLocator
import kotlinx.coroutines.flow.collectLatest
import java.util.concurrent.TimeUnit

class ProductsFragment : Fragment() {

    private val model: ProductsViewModel by viewModels {
        object : AbstractSavedStateViewModelFactory(this, null) {
            override fun <T : ViewModel?> create(
                    key: String,
                    modelClass: Class<T>,
                    handle: SavedStateHandle
            ): T {
                val repo = ServiceLocator.instance(this@ProductsFragment.requireContext()).getRepository()
                @Suppress("UNCHECKED_CAST")
                return ProductsViewModel(repo, handle) as T
            }
        }
    }

    private lateinit var binding: FragmentProductsBinding
    private lateinit var adapter: ProductsAdapter

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductsBinding.inflate(
                inflater,
                container,
                false
        )
        prepareTransitions()
        postponeEnterTransition(
                400,
                TimeUnit.MILLISECONDS
        )
        return binding.root
    }

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?
    ) {
        super.onViewCreated(
                view,
                savedInstanceState
        )
        initAdapter()
        initSwipeToRefresh()
        model.showProducts()
    }

    private fun initAdapter() {
        val glide = GlideApp.with(this)
        adapter = ProductsAdapter(
                this,
                glide
        )
        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        binding.list.adapter = adapter.withLoadStateHeaderAndFooter(
                header = ProductsLoadStateAdapter(adapter),
                footer = ProductsLoadStateAdapter(adapter)
        )

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow.collectLatest { loadStates ->
                binding.swipeRefresh.isRefreshing = loadStates.mediator?.refresh is LoadState.Loading
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            model.productItems.collectLatest {
                adapter.submitData(viewLifecycleOwner.lifecycle, it)
            }
        }
    }

    private fun initSwipeToRefresh() {
        binding.swipeRefresh.setOnRefreshListener { adapter.refresh() }
    }

    private fun prepareTransitions() {
        exitTransition = TransitionInflater.from(context).inflateTransition(R.transition.recycler_exit_transition)
        setExitSharedElementCallback(
                object : SharedElementCallback() {
                    override fun onMapSharedElements(
                            names: List<String>,
                            sharedElements: MutableMap<String, View>
                    ) {
                        val selectedViewHolder = (binding.list.findViewHolderForAdapterPosition(ProductsActivity.currentPosition)) as? ProductsViewHolder
                                ?: return
                        sharedElements[names[0]] = selectedViewHolder.binding.image
                    }
                })
    }

    companion object {
        const val TAG = "ProductsFragment"
    }
}