package com.walmart.example.products.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.commit
import com.android.example.paging.pagingwithnetwork.databinding.ActivityProductsBinding

class ProductsActivity : BaseActivity() {
    private lateinit var binding: ActivityProductsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState != null) {
            currentPosition = savedInstanceState.getInt(
                    KEY_CURRENT_POSITION,
                    0
            )
            return
        }
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            add(
                    binding.fragmentContainerView.id,
                    ProductsFragment(),
                    ProductsFragment.TAG
            )
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(
                KEY_CURRENT_POSITION,
                currentPosition
        )
    }

    companion object {
        private const val KEY_CURRENT_POSITION = "currentPosition"
        var currentPosition = 0
        fun intentFor(context: Context): Intent {
            return Intent(context, ProductsActivity::class.java)
        }
    }
}
