package com.walmart.example.products.api

import android.util.Log
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


interface ProductService {

    @GET("/walmartproducts/{pageNumber}/{pageSize}")
    suspend fun getProducts(
            @Path("pageNumber") pageNumber: Int,
            @Path("pageSize") pageSize: Int = MAX_PAGE_SIZE,
    ): ProductsResponse

    companion object {
        const val BASE_URL = "https://mobile-tha-server.firebaseapp.com"
        const val MAX_PAGE_SIZE = 30

        fun create(): ProductService {
            val logger = HttpLoggingInterceptor { Log.d("API", it) }
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .build()
            return Retrofit.Builder()
                    .baseUrl(HttpUrl.parse(BASE_URL)!!)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(ProductService::class.java)
        }
    }
}