package com.walmart.example.products.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.walmart.example.products.model.Product
import com.walmart.example.products.model.RemoteKey

@Database(
        entities = [Product::class, RemoteKey::class],
        version = 1,
        exportSchema = false
)
abstract class ProductsDb : RoomDatabase() {

    abstract fun products(): ProductsDao
    abstract fun remoteKey(): RemoteKeyDao

    companion object {
        fun create(
                context: Context,
                useInMemory: Boolean
        ): ProductsDb {
            val databaseBuilder = if (useInMemory) {
                Room.inMemoryDatabaseBuilder(
                        context,
                        ProductsDb::class.java
                )
            } else {
                Room.databaseBuilder(
                        context,
                        ProductsDb::class.java,
                        "products.db"
                )
            }
            return databaseBuilder
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }
}