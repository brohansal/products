package com.walmart.example.products.ui

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.android.example.paging.pagingwithnetwork.R
import com.android.example.paging.pagingwithnetwork.databinding.ProductsItemBinding
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.walmart.example.products.GlideRequests

class ProductsViewHolder(
        view: View,
        private val glide: GlideRequests,
        private val viewHolderListener: ProductsAdapter.ViewHolderListener
) : RecyclerView.ViewHolder(view) {

    val binding = ProductsItemBinding.bind(view)

    fun bind(productItem: ProductItem) {
        glide.load(productItem.productImage)
                .fitCenter()
                .placeholder(R.drawable.bg_product)
                .listener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable?>?,
                            isFirstResource: Boolean
                    ): Boolean {
                        viewHolderListener.onLoadCompleted(
                                binding.image,
                                bindingAdapterPosition
                        )
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?,
                                                 model: Any?,
                                                 target: Target<Drawable?>?,
                                                 dataSource: DataSource?,
                                                 isFirstResource: Boolean
                    ): Boolean {
                        viewHolderListener.onLoadCompleted(
                                binding.image,
                                bindingAdapterPosition
                        )
                        return false
                    }
                })
                .into(binding.image)
        binding.image.transitionName = productItem.productImageTransitionId
        binding.name.text = productItem.productName
        binding.rating.rating = productItem.reviewRating
        binding.price.text = productItem.price
        binding.inStock.text = binding.root.context.getString(productItem.inStockStringRes)
        binding.inStock.setTextColor(ContextCompat.getColor(
                binding.root.context,
                productItem.inStockColorRes
        ))
        binding.root.setOnClickListener {
            viewHolderListener.onItemClicked(
                    binding.root,
                    bindingAdapterPosition,
                    productItem.productId
            )
        }
    }

    fun unbind() {
        glide.clear(binding.image)
    }

    companion object {
        fun create(
                parent: ViewGroup,
                glide: GlideRequests,
                viewHolderListener: ProductsAdapter.ViewHolderListener
        ): ProductsViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(
                    R.layout.products_item,
                    parent,
                    false
            )
            return ProductsViewHolder(
                    view,
                    glide,
                    viewHolderListener
            )
        }
    }
}