package com.walmart.example.products.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.LoadType.*
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.walmart.example.products.api.ProductService
import com.walmart.example.products.db.ProductsDao
import com.walmart.example.products.db.ProductsDb
import com.walmart.example.products.db.RemoteKeyDao
import com.walmart.example.products.model.Product
import com.walmart.example.products.model.RemoteKey
import retrofit2.HttpException
import java.io.IOException

@OptIn(ExperimentalPagingApi::class)
class PageKeyedRemoteMediator(
        private val db: ProductsDb,
        private val api: ProductService,
) : RemoteMediator<Int, Product>() {
    private val productsDao: ProductsDao = db.products()
    private val remoteKeyDao: RemoteKeyDao = db.remoteKey()

    override suspend fun initialize(): InitializeAction {
        // Require that remote REFRESH is launched on initial load and succeeds before launching
        // remote PREPEND / APPEND.
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(
            loadType: LoadType,
            state: PagingState<Int, Product>
    ): MediatorResult {
        try {
            val loadKey = when (loadType) {
                REFRESH -> 1
                PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                APPEND -> {
                    val remoteKey = db.withTransaction {
                        remoteKeyDao.remoteKey()
                    }

                    if (remoteKey.nextPage == null) {
                        return MediatorResult.Success(endOfPaginationReached = true)
                    }
                    remoteKey.nextPage
                }
            }

            val data = api.getProducts(loadKey)
            val items = data.product

            db.withTransaction {
                if (loadType == REFRESH) {
                    productsDao.clearProducts()
                    remoteKeyDao.clearRemoteKey()
                }

                val pageNumber = if (loadKey == data.totalProducts) null else loadKey + 1
                remoteKeyDao.insertRemoteKey(
                        RemoteKey(
                                nextPage = pageNumber
                        )
                )
                productsDao.insertProducts(items)
            }

            return MediatorResult.Success(endOfPaginationReached = items.isEmpty())
        } catch (e: IOException) {
            return MediatorResult.Error(e)
        } catch (e: HttpException) {
            return MediatorResult.Error(e)
        }
    }
}
