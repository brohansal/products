package com.walmart.example.products.util

import com.walmart.example.products.api.ProductService
import com.walmart.example.products.model.Product

object ProductImageUtil {

    fun getResolvedProductImage(path: String): String {
        return ProductService.BASE_URL + path
    }

    fun getImageTransitionId(product: Product): String {
        return product.productImage + product.id
    }
}