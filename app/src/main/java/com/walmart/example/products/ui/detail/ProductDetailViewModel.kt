package com.walmart.example.products.ui.detail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.walmart.example.products.data.ProductRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.map

class ProductDetailViewModel(
        private val repository: ProductRepository,
        private val productId: String,
        private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val productDetailItemBuilder = ProductDetailItemBuilder()

    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    val productDetailItem = repository.getProduct(productId).map {
        productDetailItemBuilder.build(it)
    }
}