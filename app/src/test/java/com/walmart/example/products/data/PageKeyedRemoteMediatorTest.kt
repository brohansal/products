package com.walmart.example.products.data

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingConfig
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.test.core.app.ApplicationProvider
import com.walmart.example.products.db.ProductsDb
import com.walmart.example.products.model.Product
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

@ExperimentalPagingApi
@OptIn(ExperimentalCoroutinesApi::class)
class PageKeyedRemoteMediatorTest {
    private val productsFactory = ProductsFactory()
    private val mockProducts = listOf(
        productsFactory.createProduct(),
        productsFactory.createProduct(),
        productsFactory.createProduct()
    )
    private val mockDb = ProductsDb.create(
            ApplicationProvider.getApplicationContext(),
    true)

    @Test
    fun refreshLoadReturnsSuccessResultWhenMoreDataIsPresent() = runBlockingTest {

        val mockService = MockProductService().apply {
            mockProducts.forEach { post -> addProduct(post) }
        }

        val remoteMediator = PageKeyedRemoteMediator(
            mockDb,
            mockService,
        )
        val pagingState = PagingState<Int, Product>(
            listOf(),
            null,
            PagingConfig(10),
            10
        )
        val result = remoteMediator.load(
            LoadType.REFRESH,
            pagingState
        )
        assertTrue { result is RemoteMediator.MediatorResult.Success }
        assertFalse { (result as RemoteMediator.MediatorResult.Success).endOfPaginationReached }
    }

    @Test
    fun refreshLoadSuccessAndEndOfPaginationWhenNoMoreData() = runBlockingTest {
        val mockService = MockProductService()

        val remoteMediator = PageKeyedRemoteMediator(
            mockDb,
            mockService
        )
        val pagingState = PagingState<Int, Product>(
            listOf(),
            null,
            PagingConfig(10),
            10
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)
        assertTrue { result is RemoteMediator.MediatorResult.Success }
        assertTrue { (result as RemoteMediator.MediatorResult.Success).endOfPaginationReached }
    }

    @Test
    fun refreshLoadReturnsErrorResultWhenErrorOccurs() = runBlockingTest {

        val mockService = MockProductService().apply { failureMsg = "Throw test failure" }
        val remoteMediator = PageKeyedRemoteMediator(
            mockDb,
            mockService
        )
        val pagingState = PagingState<Int, Product>(
            listOf(),
            null,
            PagingConfig(10),
            10
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)
        assertTrue { result is RemoteMediator.MediatorResult.Error }
    }
}