Sample Products browsing app which has the following features :
1. Paging product list
2. Detailed product view
3. Product image viewer

Rudimentary shopping cart implementation branch : [cart](https://gitlab.com/brohansal/products/-/tree/brohan/cart)
